//
//  AppDelegate.h
//  deneme
//
//  Created by Kadircan Türker on 30/11/15.
//  Copyright © 2015 Kadircan Türker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

